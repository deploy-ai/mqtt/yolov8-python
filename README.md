# YOLOv8 project with MQTT with 3 languages: Python, Golang and C++
## Design of system
![](./docs/mqtt.jpg)

## Struct folder of system
```
.
├── cpp_pose_sub
│   ├── CMakeLists.txt
│   ├── Dockerfile
│   ├── main.cpp
│   ├── models
│   └── yolo
├── data
│   ├── det
│   ├── pose
│   └── seg
├── docker-compose.yml
├── docs
│   ├── mqtt.drawio
│   └── mqtt.jpg
├── go_det_sub
│   ├── Dockerfile
│   ├── fonts
│   ├── main.go
│   ├── models
│   ├── onnxruntime
│   └── yolov8-onnx.go
├── imgs_pub
│   ├── Dockerfile
│   ├── imgs
│   │   ├── det
│   │   ├── pose
│   │   └── seg
│   └── publish.py
├── py_seg_sub
│   ├── Dockerfile
│   ├── main.py
│   ├── models
│   └── yoloseg
└── README.md
```
## Image publisher
Using sub-folder (`det`, `pose`, `seg`) in folder `imgs` as topic name of MQTT. Read all example images in each sub-folder, decode as jpg then publish to 3 topic: `yolov8/det`, `yolov8/pose`, `yolov8/seg`

Build docker image for image publisher
```
cd imgs_pub
docker image build -t publishimg:latest .
```
## CPP pose subscriber
Subscriber topic `yolov8/pose` and using YOLOv8-pose to predict huma pose then saved result at `data/pose` mount to volume `app-volume` in docker compose.

Build docker image for cpp pose subscriber
```
cd cpp_pose_sub
docker image build -t yolov8-opencv-cpp:pose .
```

## Golang detection subscriber
Subscriber topic `yolov8/det` and using YOLOv8 to predict 80 classse COCO then saved result at `data/det` mount to volume `app-volume` in docker compose.

**Download onnxruntime**:

```
wget https://github.com/microsoft/onnxruntime/releases/download/v1.17.1/onnxruntime-linux-x64-1.17.1.tgz

tar -xzf onnxruntime-linux-x64-1.17.1.tgz -C onnxruntime

cp -r onnxruntime go_det_sub
```

Build docker image for go detection subscriber
```
cd cd go_det_sub
docker image build -t yolov8-onnx-go:det .
```
## Python segmentation subscriber
Subscriber topic `yolov8/seg` and using YOLOv8 to segmentation 80 classes COCO then saved result at `data/seg` mount to volume `app-volume` in docker compose.


Build docker image for go detection subscriber
```
cd cd py_seg_sub
docker image build -t yolov8-onnx-py:seg .
```

## Download and export YOLOv8 model
Follow instruction at [ultralytics](https://github.com/ultralytics/ultralytics)

```
pip install ultralytics

yolo export model=yolov8s.pt format=onnx simplify=true opset=11
```

## Run system

- Edit `device` in `docker-compose.yml` to specify folder for saved results
  ```
  volumes:
    app-volume:
      driver: local
      driver_opts:
        type: none
        device: /home/user/yolov8-mqtt/data
        o: bind
  ```
- Changed model and set threshold
  ```
    YOLOV8_MODEL: "yolov8s-pose.onnx"
    CONFIDENCE_THRESHOLD: 0.35
  ```

- Run system
  ```
  # build all docker images
  make

  # start
  docker compose up

  # stop
  docker compose down
  ```