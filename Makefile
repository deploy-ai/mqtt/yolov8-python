.PHONY: all go_det_sub imgs_pub py_seg_sub cpp_pose_sub

all: go_det_sub imgs_pub py_seg_sub cpp_pose_sub

go_det_sub:
    docker build -t yolov8-onnx-go:det -f go_det_sub/Dockerfile go_det_sub

imgs_pub:
    docker build -t publishimg:latest -f imgs_pub/Dockerfile imgs_pub

py_seg_sub:
    docker build -t yolov8-onnx-py:seg -f py_seg_sub/Dockerfile py_seg_sub

cpp_pose_sub:
	docker build -t yolov8-opencv-cpp:pose -f cpp_pose_sub/Dockerfile cpp_pose_sub