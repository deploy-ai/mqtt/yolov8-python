import cv2
import os
import time

import paho.mqtt.client as mqtt

def on_connect(mqttc, obj, flags, reason_code, properties):
    print("Connect to Broker: " + str(reason_code))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid, reason_code, properties):
    print("Publish " + str(reason_code))


def on_log(mqttc, obj, level, string):
    print(string)

if __name__ == "__main__":

    clientid    = os.environ.get("CLIENT_ID")
    username    = os.environ.get("USER_NAME")
    password    = os.environ.get("PASS_WORD")
    host        = os.environ.get("BROKER")
    port        = int(os.environ.get("PORT"))
    keepalive   = 60

    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, clientid, clean_session = True)

    mqttc.username_pw_set(username, password)

    mqttc.on_message = on_message
    mqttc.on_connect = on_connect
    mqttc.on_publish = on_publish

    print("Connecting to "+host+" port: "+str(port))
    mqttc.connect(host, port, keepalive)

    mqttc.loop_start()

    for root, dirs, files in os.walk("/app/imgs"):
        sub_path = root.split("/")[-1]
        topic = "yolov8/" + sub_path

        for file in files:
            if file.endswith(('.png', '.jpg')):
                img_path = os.path.join(root, file)
                img = cv2.imread(img_path)

                retval, buffer = cv2.imencode('.jpg', img)
                image_bytes = buffer.tobytes()
                print(f"Publish image to topic: {topic}")
                infot = mqttc.publish(topic, image_bytes, qos=1)
                infot.wait_for_publish()

                time.sleep(1)

    mqttc.disconnect()