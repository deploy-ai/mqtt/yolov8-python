#ifndef YOLOV8_YOLOPOSE_H
#define YOLOV8_YOLOPOSE_H

#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>

/*

{
    'names': ['person'],
    'boxes': tensor([[x1, y1, x2, y2, conf, cls_idx]]),
    'kp': tensor([[x1_kpt_0, y1_kpt_0, score_0], ... [x1_kpt_n, y1_kpt_n, score_n]])
}

*/

class YoloPose {
private:
    cv::dnn::Net net;
    const cv::Size modelShape = cv::Size(640, 640);

    const float modelScoreThreshold;
    const float modelNMSThreshold;

public:
    YoloPose(const std::string &modelPath, float confThres=0.5, float nmsThres=0.5);

    struct Keypoint {
        Keypoint(float x, float y, float score);

        cv::Point2d position{};
        float conf{0.0};
    };

    struct Person {
        Person(cv::Rect2i _box, float _score, std::vector<Keypoint> &_kp);

        cv::Rect2i box{};
        float score{0.0};
        std::vector<Keypoint> kp{};
    };

    std::vector<Person> detect(cv::Mat &mat);

    static void show(cv::Mat &image);

    static void draw(std::vector<YoloPose::Person> &detections, cv::Mat &image);
};

inline static float clamp(float val, float min, float max);

#endif //YOLOV8_YOLOPOSE_H
