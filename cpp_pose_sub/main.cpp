#include <iostream>
#include <ctime>
#include <sstream>
#include <thread>
#include <chrono>
#include <vector>
#include <cstdlib>
#include "mqtt/client.h"
#include "opencv2/opencv.hpp"
#include "yolo/YoloPose.h"

using namespace std;
using namespace std::chrono;

/////////////////////////////////////////////////////////////////////////////
// Get current time as Unix timestamp

std::string getUnixTime()
{
	std::time_t unixTime = std::time(nullptr);

    std::stringstream ss;
    ss << unixTime;
    return ss.str();
}

int main(int argc, char* argv[])
{
	const std::string host 		= std::string(std::getenv("BROKER"));
	const std::string port 		= std::string(std::getenv("PORT"));
	const std::string username	= std::string(std::getenv("USER_NAME"));
    const std::string password  = std::string(std::getenv("PASS_WORD"));
	const std::string clientID  = std::string(std::getenv("CLIENT_ID"));
	const std::string topic     = std::string(std::getenv("TOPIC"));
	const std::string modelname = std::string(std::getenv("YOLOV8_MODEL"));
	const std::string confthres = std::string(std::getenv("CONFIDENCE_THRESHOLD"));

	std::string Broker = host+ ":" + std::string(port);
	std::string Client_ID = std::string(clientID);
	mqtt::client cli(Broker, Client_ID);

	auto connOpts = mqtt::connect_options_builder()
		.user_name(username)
		.password(password)
		.keep_alive_interval(seconds(30))
		.automatic_reconnect(seconds(2), seconds(30))
		.clean_session(true)
		.finalize();

	// You can install a callback to change some connection data
	// on auto reconnect attempts. To make a change, update the
	// `connect_data` and return 'true'.
	cli.set_update_connection_handler(
		[](mqtt::connect_data& connData) {
			string newUserName { "newuser" };
			if (connData.get_user_name() == newUserName)
				return false;

			cout << "Previous user: '" << connData.get_user_name()
				<< "'" << endl;
			connData.set_user_name(newUserName);
			cout << "New user name: '" << connData.get_user_name()
				<< "'" << endl;
			return true;
		}
	);

	const int QOS = 1;

	try {
		cout << "Connecting to the MQTT broker " <<host << flush;
		mqtt::connect_response rsp = cli.connect(connOpts);
		cout << " - OK\n" << endl;

		if (!rsp.is_session_present()) {
			std::cout << "Subscribing to topics " << topic << std::flush;
			cli.subscribe(topic, QOS);
			std::cout << " - OK" << std::endl;
		}
		else {
			cout << "Session already present. Skipping subscribe." << std::endl;
		}

		// Consume messages

		while (true) {
			auto msg = cli.consume_message();

			if (msg) 
			{
				cout << "Topic:" << msg->get_topic() << endl;

				const std::string payload = msg->get_payload();
				std::vector<uchar> buffer(payload.begin(), payload.end());
				// Decode the JPEG image
				cv::Mat img = cv::imdecode(buffer, cv::IMREAD_COLOR);

				if (img.empty()) {
					std::cerr << "Error decoding image" << std::endl;
					return 1;
				}

				std::string modelPath = "/app/cpp/models/" + modelname;
				auto yoloPose = YoloPose(modelPath, std::stof(confthres), 0.5);

				std::vector<YoloPose::Person> results = yoloPose.detect(img);
				yoloPose.draw(results, img);

				std::string fileName = "/data/pose/" + getUnixTime() +".jpg";
				cv::imwrite(fileName, img);
			}
			else if (!cli.is_connected()) {
				cout << "Lost connection" << endl;
				while (!cli.is_connected()) {
					this_thread::sleep_for(milliseconds(250));
				}
				cout << "Re-established connection" << endl;
			}
		}

		// Disconnect

		cout << "\nDisconnecting from the MQTT server..." << flush;
		cli.disconnect();
		cout << "OK" << endl;
	}
	catch (const mqtt::exception& exc) {
		cerr << exc.what() << endl;
		return 1;
	}

 	return 0;
}
