package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"
	"math/rand"
	"os"
	"sort"
	"strconv"

	"github.com/StephaneBunel/bresenham"
	"github.com/golang/freetype/truetype"
	"github.com/nfnt/resize"
	ort "github.com/yalue/onnxruntime_go"
	"golang.org/x/image/font"
	"golang.org/x/image/math/fixed"
)

var yolo_classes = []string{
	"person", "bicycle", "car", "motorcycle", "airplane", "bus", "train", "truck", "boat",
	"traffic light", "fire hydrant", "stop sign", "parking meter", "bench", "bird", "cat", "dog", "horse",
	"sheep", "cow", "elephant", "bear", "zebra", "giraffe", "backpack", "umbrella", "handbag", "tie",
	"suitcase", "frisbee", "skis", "snowboard", "sports ball", "kite", "baseball bat", "baseball glove",
	"skateboard", "surfboard", "tennis racket", "bottle", "wine glass", "cup", "fork", "knife", "spoon",
	"bowl", "banana", "apple", "sandwich", "orange", "broccoli", "carrot", "hot dog", "pizza", "donut",
	"cake", "chair", "couch", "potted plant", "bed", "dining table", "toilet", "tv", "laptop", "mouse",
	"remote", "keyboard", "cell phone", "microwave", "oven", "toaster", "sink", "refrigerator", "book",
	"clock", "vase", "scissors", "teddy bear", "hair drier", "toothbrush",
}

func detect_objects_on_image(img image.Image, modelName string, confThres float32) [][]interface{} {
	input, img_width, img_height := prepare_input(img)
	output := run_model(input, modelName)
	return process_output(output, img_width, img_height, confThres)
}

func prepare_input(img image.Image) ([]float32, int64, int64) {
	// img, _, _ := image.Decode(buf)
	size := img.Bounds().Size()
	img_width, img_height := int64(size.X), int64(size.Y)
	img = resize.Resize(640, 640, img, resize.Lanczos3)
	red := []float32{}
	green := []float32{}
	blue := []float32{}
	for y := 0; y < 640; y++ {
		for x := 0; x < 640; x++ {
			r, g, b, _ := img.At(x, y).RGBA()
			red = append(red, float32(r/257)/255.0)
			green = append(green, float32(g/257)/255.0)
			blue = append(blue, float32(b/257)/255.0)
		}
	}
	input := append(red, green...)
	input = append(input, blue...)
	return input, img_width, img_height
}

func run_model(input []float32, modelName string) []float32 {
	onnxLibPath := "/app/go/onnxruntime/lib/libonnxruntime.so"
	if _, err := os.Stat(onnxLibPath); err != nil {
		fmt.Printf("File libonnxruntime.so does not exist\n")
		os.Exit(1)
	}
	ort.SetSharedLibraryPath(onnxLibPath)
	_ = ort.InitializeEnvironment()

	inputShape := ort.NewShape(1, 3, 640, 640)
	inputTensor, _ := ort.NewTensor(inputShape, input)

	outputShape := ort.NewShape(1, 84, 8400)
	outputTensor, _ := ort.NewEmptyTensor[float32](outputShape)

	onnxModelPath := "/app/go/models/" + modelName
	if _, err := os.Stat(onnxModelPath); err != nil {
		fmt.Printf("File %s does not exist\n", modelName)
		os.Exit(1)
	}
	model, _ := ort.NewSession[float32](onnxModelPath,
		[]string{"images"}, []string{"output0"},
		[]*ort.Tensor[float32]{inputTensor}, []*ort.Tensor[float32]{outputTensor})

	_ = model.Run()
	return outputTensor.GetData()
}

func process_output(output []float32, img_width, img_height int64, confThres float32) [][]interface{} {
	boxes := [][]interface{}{}
	for index := 0; index < 8400; index++ {
		class_id, prob := 0, float32(0.0)
		for col := 0; col < 80; col++ {
			if output[8400*(col+4)+index] > prob {
				prob = output[8400*(col+4)+index]
				class_id = col
			}
		}
		if prob < confThres {
			continue
		}
		labelID := class_id
		xc := output[index]
		yc := output[8400+index]
		w := output[2*8400+index]
		h := output[3*8400+index]
		x1 := (xc - w/2) / 640 * float32(img_width)
		y1 := (yc - h/2) / 640 * float32(img_height)
		x2 := (xc + w/2) / 640 * float32(img_width)
		y2 := (yc + h/2) / 640 * float32(img_height)
		boxes = append(boxes, []interface{}{float64(x1), float64(y1), float64(x2), float64(y2), int(labelID), float64(prob)})
	}

	sort.Slice(boxes, func(i, j int) bool {
		return boxes[i][5].(float64) < boxes[j][5].(float64)
	})

	// NMS
	result := [][]interface{}{}
	for len(boxes) > 0 {
		result = append(result, boxes[0])
		tmp := [][]interface{}{}
		for _, box := range boxes {
			if iou(boxes[0], box) < 0.7 {
				tmp = append(tmp, box)
			}
		}
		boxes = tmp
	}
	return result
}

func iou(box1, box2 []interface{}) float64 {
	return intersection(box1, box2) / union(box1, box2)
}

func union(box1, box2 []interface{}) float64 {
	box1_x1, box1_y1, box1_x2, box1_y2 := box1[0].(float64), box1[1].(float64), box1[2].(float64), box1[3].(float64)
	box2_x1, box2_y1, box2_x2, box2_y2 := box2[0].(float64), box2[1].(float64), box2[2].(float64), box2[3].(float64)
	box1_area := (box1_x2 - box1_x1) * (box1_y2 - box1_y1)
	box2_area := (box2_x2 - box2_x1) * (box2_y2 - box2_y1)
	return box1_area + box2_area - intersection(box1, box2)
}

func intersection(box1, box2 []interface{}) float64 {
	box1_x1, box1_y1, box1_x2, box1_y2 := box1[0].(float64), box1[1].(float64), box1[2].(float64), box1[3].(float64)
	box2_x1, box2_y1, box2_x2, box2_y2 := box2[0].(float64), box2[1].(float64), box2[2].(float64), box2[3].(float64)
	x1 := math.Max(box1_x1, box2_x1)
	y1 := math.Max(box1_y1, box2_y1)
	x2 := math.Min(box1_x2, box2_x2)
	y2 := math.Min(box1_y2, box2_y2)
	return (x2 - x1) * (y2 - y1)
}

func generateRandomColors(n int) []color.RGBA {
	colors := make([]color.RGBA, n)
	for i := 0; i < n; i++ {
		colors[i] = color.RGBA{
			R: uint8(rand.Intn(256)),
			G: uint8(rand.Intn(256)),
			B: uint8(rand.Intn(256)),
			A: 255,
		}
	}
	return colors
}

func drawText(img draw.Image, x, y int, text string, face font.Face, col color.Color) {
	d := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(col),
		Face: face,
		Dot: fixed.Point26_6{
			X: fixed.Int26_6(x * 64),
			Y: fixed.Int26_6(y * 64),
		},
	}
	d.DrawString(text)
}

func drawRectangle(img draw.Image, x1, y1, x2, y2 int, col color.Color) {
	// Draw the four lines of the rectangle
	bresenham.DrawLine(img, x1, y1, x2, y1, col)
	bresenham.DrawLine(img, x1, y1, x1, y2, col)
	bresenham.DrawLine(img, x1, y2, x2, y2, col)
	bresenham.DrawLine(img, x2, y1, x2, y2, col)
}

func drawObject(img image.Image, objects [][]interface{}) draw.Image {
	// fmt.Println("size images: ", img.Bounds().Size())
	rgba := image.NewRGBA(img.Bounds())
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{}, draw.Src)

	// Load font
	fontBytes, _ := os.ReadFile("/app/go/fonts/luxi-sans-regular.ttf")

	font, _ := truetype.Parse(fontBytes)

	const fontSize = 24
	colors := generateRandomColors(80)

	// Create the font face
	face := truetype.NewFace(font, &truetype.Options{
		Size: fontSize,
		DPI:  72,
	})
	for _, object := range objects {
		x1 := object[0].(float64)
		y1 := object[1].(float64)
		x2 := object[2].(float64)
		y2 := object[3].(float64)
		ID := object[4].(int)

		color := colors[ID]
		drawRectangle(rgba, int(x1), int(y1), int(x2), int(y2), color)

		text := yolo_classes[ID] + ": " + strconv.FormatFloat(object[5].(float64), 'f', 2, 64)

		drawText(rgba, int(x1), int(y1), text, face, color)
	}

	return rgba
}
