package main

import (
	"bytes"

	"fmt"
	"image/jpeg"

	"os"
	"os/signal"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

func createMessagePubHandler(modelName string, confThres float32) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		fmt.Println("Topic: ", msg.Topic())

		buffer := bytes.NewReader(msg.Payload())

		// Decode the JPEG image
		img, err := jpeg.Decode(buffer)
		if err != nil {
			fmt.Println("Error decoding JPEG image:", err)
			return
		}

		objects := detect_objects_on_image(img, modelName, confThres)
		// for _, object := range objects {
		// 	x1 := object[0].(float64)
		// 	y1 := object[1].(float64)
		// 	x2 := object[2].(float64)
		// 	y2 := object[3].(float64)

		// 	color := color.RGBA{255, 0, 0, 255}
		// 	drawRectangle(rgba, int(x1), int(y1), int(x2), int(y2), color)

		// 	// draw.Draw(rgbaImg, rect.Inset(2), &image.Uniform{color}, image.Point{}, draw.Src)
		// 	text := object[4].(string) + ": " + strconv.FormatFloat(object[5].(float64), 'f', 2, 64)

		// 	drawText(rgba, int(x1), int(y1), text, face, color)
		// }
		rgba := drawObject(img, objects)
		// Save the image to a file
		namePath := "/data/det/" + strconv.FormatInt(time.Now().Unix(), 10) + ".jpg"
		outputFile, err := os.Create(namePath)
		if err != nil {
			fmt.Println("Error:", err)
			return
		}
		defer outputFile.Close()

		jpeg.Encode(outputFile, rgba, nil)
	}
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected to broker Success")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %+v", err)
}

func main() {
	broker := os.Getenv("BROKER")
	port := os.Getenv("PORT")
	topic := os.Getenv("TOPIC")
	clientID := os.Getenv("CLIENT_ID")
	userName := os.Getenv("USER_NAME")
	passWord := os.Getenv("PASS_WORD")
	modelName := os.Getenv("YOLOV8_MODEL")
	confThresStr := os.Getenv("CONFIDENCE_THRESHOLD")
	confThres, _ := strconv.ParseFloat(confThresStr, 32)

	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s:%s", broker, port))
	opts.SetClientID(clientID)
	opts.SetUsername(userName)
	opts.SetPassword(passWord)

	opts.SetKeepAlive(60)
	opts.OnConnect = connectHandler
	opts.OnConnectionLost = connectLostHandler

	client := mqtt.NewClient(opts)
	token := client.Connect()
	if token.WaitTimeout(3*time.Second) && token.Error() != nil {
		panic(token.Error())
	}

	sub(client, topic, modelName, float32(confThres))

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c

	client.Disconnect(250)
}

func sub(client mqtt.Client, topic string, modelName string, confThres float32) {
	// Subscribe to the LWT connection status
	messagePubHandler := createMessagePubHandler(modelName, confThres)
	token := client.Subscribe(topic, 1, messagePubHandler)
	token.Wait()
	fmt.Printf("Subscribed to topic %s \n", topic)
}
