import os
import cv2
import time
import numpy as np
import logging
from yoloseg import YOLOSeg
import paho.mqtt.client as mqtt

class MQTTClient(mqtt.Client):
    def __init__(self, model_path, conf_thres):
        self.yoloSeg = YOLOSeg(model_path, conf_thres)


    def on_connect(self, mqttc, obj, flags, reason_code, properties):
        logging.info("Connect to broker: " + str(reason_code))


    def on_message(self, mqttc, obj, msg):
        msg.topic
        image_bytes = msg.payload
        image_array = np.frombuffer(image_bytes, dtype=np.uint8)
        img = cv2.imdecode(image_array, cv2.IMREAD_COLOR)

        img_size = img.shape[:2]

        logging.info("size of image: ", img_size)

        boxes, scores, class_ids, masks = self.yoloSeg(img)

        mask_img = self.yoloSeg.draw_masks(img)

        img_path = f"/data/seg/{int(time.time())}.jpg"
        cv2.imwrite(img_path, mask_img)

    def on_publish(self, mqttc, obj, mid):
        logging.info("mid: " + str(mid))


    def on_subscribe(self, mqttc, obj, mid, reason_code_list, properties):
        logging.info("Subscribed: " + str(mid) + " " + str(reason_code_list))


    def on_log(self, mqttc, obj, level, string):
        logging.info(string)

if __name__ == "__main__":
    client_id    = os.environ.get("CLIENT_ID")
    username    = os.environ.get("USER_NAME")
    password    = os.environ.get("PASS_WORD")
    host        = os.environ.get("BROKER")
    port        = os.environ.get("PORT")
    topic       = os.environ.get("TOPIC")
    conf_thres  = float(os.environ.get("CONFIDENCE_THRESHOLD"))
    model_name  = os.environ.get("YOLOV8_MODEL")
    keepalive   = 60

    model_path = "/app/models/" + model_name

    client_yolo = MQTTClient(model_path, conf_thres)
    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2, client_id, clean_session = True)

    mqttc.on_message = client_yolo.on_message
    mqttc.on_connect = client_yolo.on_connect
    mqttc.on_publish = client_yolo.on_publish
    mqttc.on_subscribe = client_yolo.on_subscribe


    print("Connecting to "+ host+" port: " + port)
    mqttc.connect(host, int(port), keepalive)
    mqttc.subscribe(topic, 1)

    mqttc.loop_forever()